export default (httpRequest) => ({
  getExchangeRates: () => httpRequest.get('/exchange-rates'),
  getExchangeHistory: (q = '') => httpRequest.get(`/exchange-rates/history${q}`),
});
