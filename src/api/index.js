import request from '@/service/request';
import conversion from '@/api/conversion';

const ajax = () => ({
  conversion: conversion(request),
});

export default ajax();
