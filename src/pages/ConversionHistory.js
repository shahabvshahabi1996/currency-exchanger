import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { dateFormat } from '@/utils/datetime';
import { useStateToProps, useDispatchToProps } from '@/redux/actions';
import { DELETE_HISTORY } from '@/redux/actions/action-types';
import history from '@/utils/history';
import queryMaker from '@/utils/queryMaker';
import Layout from '@/components/common/Layout/Layout';
import Table from '@/components/common/Table/Table';

export default function ConversionHistory() {
  const { t } = useTranslation();
  const deleteExchange = useDispatchToProps(DELETE_HISTORY);
  const exchangeHistory = useStateToProps((state) => state.exchanges.history);
  const list = useMemo(() => (exchangeHistory || []).reverse().map((exchange) => ({
    ...exchange,
    date: dateFormat(exchange.date, 'DD/MM/YYYY @ hh:mm'),
  })), [exchangeHistory]);

  const fieldsConfig = [
    {
      id: 1,
      name: 'date',
      title: t('date'),
    },
    {
      id: 2,
      name: 'event',
      text: ({ amount, from, to }) => t('exchangeHistoryEvent', [amount, from.currency, to.currency]),
    },
    {
      id: 3,
      name: 'actions',
      title: t('actions'),
      type: 'action',
      actions: [
        {
          text: t('view'),
          icon: <VisibilityIcon />,
          className: 'view',
          click: ({ amount, from, to }) => {
            history.push(`/?${queryMaker({
              amount,
              from: JSON.stringify(from),
              to: JSON.stringify(to),
            })}`);
          },
        },
        {
          text: t('delete'),
          icon: <DeleteForeverIcon />,
          className: 'danger',
          click: (row) => { deleteExchange(row.id); },
        },
      ],
    },
  ];

  return (
    <Layout>
      <div className="padding-md-top margin-extra-top">
        <Typography
          variant="h1"
        >
          { t('currencyConversion') }
        </Typography>
        <div className="margin-md-top">
          <Grid container>
            <Grid item xs={12}>
              <Table
                fieldsConfig={fieldsConfig}
                list={list}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    </Layout>
  );
}
