import React from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@material-ui/core/Typography';
import Layout from '@/components/common/Layout/Layout';

export default function NotFound() {
  const { t } = useTranslation();
  return (
    <Layout>
      <div className="padding-md-top margin-extra-top">
        <Typography
          variant="h1"
        >
          { t('notFound') }
        </Typography>
      </div>
    </Layout>
  );
}
