import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Layout from '@/components/common/Layout/Layout';
import Converter from '@/components/pages/converter/Converter';
import ConversionResult from '@/components/pages/converter/ConversionResult/ConversionResult';
import ExchangeHistory from '@/components/pages/converter/ExchangeHistory';
import { useDispatchToProps } from '@/redux/actions';
import { SET_HISTORY } from '@/redux/actions/action-types';

export default function CurrencyConverter() {
  const { t } = useTranslation();
  const setExchangeHistory = useDispatchToProps(SET_HISTORY);
  const [result, setResult] = useState({});

  const onSubmitForm = (value) => {
    const { amount, from, to } = value;
    const date = new Date().toISOString();
    setExchangeHistory({
      id: date,
      date,
      from,
      to,
      amount,
    });
    setResult(value);
  };

  const isObjectEmpty = (obj) => !!Object.keys(obj).length;

  return (
    <Layout>
      <div className="padding-md-top margin-extra-top">
        <Typography
          variant="h1"
        >
          { t('iWantToConvert') }
        </Typography>
        <div className="margin-md-top">
          <Converter
            onSubmit={onSubmitForm}
          />
          {
          isObjectEmpty(result)
            && (
              <>
                <ConversionResult
                  amount={result.amount}
                  from={result.from}
                  to={result.to}
                />
                <div className="margin-xlg-top" />
                <Divider />
                <ExchangeHistory from={result.from} to={result.to} />
              </>
            )
          }
        </div>
      </div>
    </Layout>
  );
}
