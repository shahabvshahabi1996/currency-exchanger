export default {
  typography: {
    h1: {
      fontSize: 48,
      fontWeight: 700,
    },
    subtitle1: {
      fontSize: 24,
      fontWeight: 700,
    },
    subtitle2: {
      fontSize: 16,
      fontWeight: 500,
    },
    body: {
      fontSize: 16,
      fontWeight: 500,
    },
    button: {
      fontSize: 16,
      fontWeight: 100,
    },
  },
  palette: {
    primary: {
      main: '#009688',
    },
    text: {
      main: '#404040',
    },
    accent: {
      main: '#04C720',
    },
    warn: {
      main: '#C70D38',
    },
    tableHeader: {
      main: '#8D8D8D',
    },
  },
  shape: {
    borderRadius: 2,
  },
};
