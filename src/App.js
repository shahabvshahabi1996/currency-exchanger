import React from 'react';
import { Provider } from 'react-redux';
import ThemeProvider from '@/components/providers/ThemeProvider';
import store from '@/redux/store';
import Boot from '@/Boot';

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider>
        <Boot />
      </ThemeProvider>
    </Provider>
  );
}

export default App;
