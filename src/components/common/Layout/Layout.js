import React from 'react';
import PropTypes from 'prop-types';
import LinearProgress from '@/components/common/LinearProgress/LinearProgress';
import Navbar from '@/components/common/Navbar/Navbar';
import Content from '@/components/common/Content/Content';
import useStyles from '@/components/common/Layout/styles.layout';

export default function Layout({ children }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <LinearProgress />
      <Navbar />
      <Content>
        {children}
      </Content>
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
