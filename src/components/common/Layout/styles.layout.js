import { makeStyles } from '@material-ui/core';
import grey from '@material-ui/core/colors/grey';

export default makeStyles((theme) => ({
  root: {
    fontFamily: theme.typography.fontFamily,
    backgroundColor: grey[300],
    height: '100vh',
    width: '100vw',
    overflowX: 'hidden',
  },
}));
