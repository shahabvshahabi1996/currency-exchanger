import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  table: {
    width: '100%',
    '& .table-header': {
      color: theme.palette.tableHeader.main,
      fontWeight: 700,
    },
    '& .table-row': {
      height: theme.spacing(9),
    },
  },
  danger: {
    color: theme.palette.warn.main,
    fontSize: theme.typography.caption.fontSize,
    fontWeight: 500,
  },
  view: {
    color: theme.palette.primary.light,
    fontSize: theme.typography.caption.fontSize,
    fontWeight: 500,
  },
}));
