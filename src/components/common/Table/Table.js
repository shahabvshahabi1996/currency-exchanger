import React, { useState } from 'react';
import PropTypes from 'prop-types';
import TableComponent from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import useStyles from '@/components/common/Table/styles.table';

export default function Table({ fieldsConfig, list }) {
  const classes = useStyles();
  const [hoveredElement, setHoverElement] = useState(null);

  const renderRow = (config, item) => {
    switch (config.type) {
      case 'action':
        return (
          <TableCell key={config.id} align="left" width={300}>
            <Grid container spacing={2}>
              {hoveredElement === item.id && (config?.actions || []).map((action) => (
                <Grid item key={action.text}>
                  <Button
                    className={classes[action.className]}
                    startIcon={action.icon}
                    onClick={() => action.click(item)}
                  >
                    {action.text}
                  </Button>
                </Grid>
              ))}
            </Grid>
          </TableCell>
        );

      default:
        return (
          <TableCell key={config.id} align="left">{config.text ? config.text(item) : item[config.name]}</TableCell>
        );
    }
  };

  return (
    <TableContainer component={Paper}>
      <TableComponent className={classes.table}>
        <TableHead>
          <TableRow>
            {fieldsConfig.map((config) => <TableCell className="table-header" key={config.id} align="left">{config.title}</TableCell>)}
          </TableRow>
        </TableHead>
        <TableBody>
          {list.length ? list.map((item) => (
            <TableRow
              key={item.id}
              onMouseEnter={() => setHoverElement(item.id)}
              onMouseLeave={() => setHoverElement(null)}
              className="table-row"
            >
              {fieldsConfig.map((config) => (renderRow(config, item)))}
            </TableRow>
          )) : []}
        </TableBody>
      </TableComponent>
    </TableContainer>
  );
}

Table.propTypes = {
  fieldsConfig: PropTypes.arrayOf(PropTypes.object).isRequired,
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
};
