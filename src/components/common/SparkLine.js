import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  Chart,
  CategoryScale,
  LineController,
  LineElement,
  PointElement,
  LinearScale,
  Title,
} from 'chart.js';
import Paper from '@material-ui/core/Paper';
import { useTheme } from '@material-ui/core';

Chart.register(CategoryScale, LineController, LineElement, PointElement, LinearScale, Title);

export default function SparkLine({ labels, list }) {
  const canvas = useRef(null);
  const theme = useTheme();
  useEffect(() => {
    const chart = new Chart(canvas.current, {
      type: 'line',
      data: {
        labels,
        datasets: [
          {
            backgroundColor: theme.palette.primary.main,
            data: list,
          },
        ],
      },
      options: {
        responsive: true,
        legend: {
          display: true,
        },
        elements: {
          line: {
            borderColor: theme.palette.primary.main,
            borderWidth: 2,
          },
          point: {
            radius: 5,
          },
        },
        tooltips: {
          enabled: true,
        },
        scales: {
          y: {
            grid: {
              color: theme.palette.tableHeader.main,
            },
          },
          x: {
            grid: {
              color: theme.palette.tableHeader.main,
            },
          },
        },
        layout: {
          padding: 20,
        },
      },
    });
    return () => {
      chart.destroy();
    };
  }, [labels, list, theme.palette.primary.main, theme.palette.tableHeader.main]);

  return (
    <Paper>
      <canvas ref={canvas} />
    </Paper>
  );
}

SparkLine.propTypes = {
  list: PropTypes.arrayOf(PropTypes.any).isRequired,
  labels: PropTypes.arrayOf(PropTypes.any).isRequired,
};
