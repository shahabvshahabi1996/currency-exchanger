import React from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import DrawerComponent from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import NavigationLink from '@/components/common/NavigationLink/NavigationLink';
import useStyles from '@/components/common/Drawer/styles.drawer';

export default function Drawer({ open, toggle }) {
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <DrawerComponent anchor="left" open={open} onClose={() => toggle(false)}>
      <div
        className={classes.root}
      >
        <Typography className={classes.title} variant="h6">
          <span className="currency">{ t('currency') }</span>
          { t('exchange') }
        </Typography>
        <div>
          <NavigationLink to="/" active>
            { t('currencyConverter') }
          </NavigationLink>
        </div>
        <div className="margin-lg-top">
          <NavigationLink to="/history">
            { t('viewConversionHistory') }
          </NavigationLink>
        </div>
      </div>
    </DrawerComponent>
  );
}

Drawer.propTypes = {
  open: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
};
