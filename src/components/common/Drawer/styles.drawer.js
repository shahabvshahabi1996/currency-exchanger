import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    width: 240,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2),
  },
  title: {
    marginBottom: theme.spacing(4),
    color: theme.palette.text.primary,
    '& > .currency': {
      color: theme.palette.tableHeader.main,
      fontWeight: 300,
    },
  },
}));
