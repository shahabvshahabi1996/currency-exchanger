import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(
  {
    root: {
      width: '100%',
      position: 'absolute',
      top: 0,
      left: 0,
      zIndex: 10000,
    },
    show: {
      display: 'initial',
    },
    hide: {
      display: 'none',
    },
  },
);
