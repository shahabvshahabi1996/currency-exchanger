import React, { useEffect, useRef } from 'react';
import ProgressBar from '@material-ui/core/LinearProgress';
import { useStateToProps } from '@/redux/actions/index';
import useStyles from '@/components/common/LinearProgress/styles.linear-progress';

export default function LinearProgress() {
  const classes = useStyles();
  const timer = useRef(null);
  const progress = useRef(0);
  const isLoading = useStateToProps((state) => state.settings.isLoading);

  const start = () => {
    timer.current = setInterval(() => {
      progress.current = Math.min(progress.current + 2, 100);
    }, 500);
  };

  const finish = () => {
    clearInterval(timer.current);
    timer.current = null;
  };

  useEffect(() => {
    if (isLoading) start();
    else finish();
  }, [isLoading]);

  return (
    <div className={`${classes.root} ${isLoading ? classes.show : classes.hide}`}>
      <ProgressBar variant="determinate" value={progress.current} />
    </div>
  );
}
