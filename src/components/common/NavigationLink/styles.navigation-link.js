import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  link: {
    textDecoration: 'none',
    padding: theme.spacing(2.5, 0),
    marginRight: theme.spacing(2),
    color: theme.palette.tableHeader.main,
    fontWeight: 500,
    borderBottom: '4px solid transparent',
    transition: 'all 0.3s',
    '& > *': {
      ...theme.typography.body2,
    },
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(1, 0),
      margin: 0,
      '& > *': {
        ...theme.typography.caption,
      },
    },
  },
  active: {
    color: theme.palette.text.primary,
    borderBottom: `4px solid ${theme.palette.primary.main}`,
  },
}));
