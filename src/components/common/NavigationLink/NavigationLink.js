import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import PropTypes from 'prop-types';
import useStyles from '@/components/common/NavigationLink/styles.navigation-link';

export default function NavigationLink({ to, children }) {
  const classes = useStyles();
  const match = useRouteMatch({
    path: to,
    exact: true,
  });
  return (
    <Link
      to={to}
      className={`${classes.link} ${match && classes.active}`}
    >
      <span>{children}</span>
    </Link>
  );
}

NavigationLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};
