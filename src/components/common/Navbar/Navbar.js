import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useMediaQuery, useTheme } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import TranslateIcon from '@material-ui/icons/Translate';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import Brightness7Icon from '@material-ui/icons/Brightness7';
import MenuIcon from '@material-ui/icons/Menu';
import { setLanguage, getLanguageName, languages } from '@/i18n';
import { useDispatchToProps, useStateToProps } from '@/redux/actions';
import { SET_THEME } from '@/redux/actions/action-types';
import NavigationLink from '@/components/common/NavigationLink/NavigationLink';
import Dropdown from '@/components/common/Dropdown';
import Drawer from '@/components/common/Drawer/Drawer';
import useStyles from '@/components/common/Navbar/styles.navbar';

export default function Navbar() {
  const classes = useStyles();
  const { t } = useTranslation();
  const languageOptions = Object
    .keys(languages)
    .reduce(
      (prev, current) => [...prev, { title: t(languages[current].name), value: current }],
      [],
    );
  const storedTheme = useStateToProps((state) => state.settings.theme);
  const storedLang = useStateToProps((state) => state.settings.lang);
  const langName = getLanguageName(storedLang);
  const [showDrawer, setDrawerVisibility] = useState(false);

  const toggleTheme = useDispatchToProps(SET_THEME);
  const themeVariable = useTheme();
  const isDesktop = useMediaQuery(themeVariable.breakpoints.up('md'));

  const changeLanguage = (lang) => {
    setLanguage(lang);
  };

  const changeTheme = () => {
    const theme = storedTheme === 'light'
      ? 'dark'
      : 'light';
    toggleTheme(theme);
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.navbar}>
        <div className={classes.toolbar}>
          <div>
            {!isDesktop && (
              <IconButton
                onClick={() => setDrawerVisibility(true)}
              >
                <MenuIcon />
              </IconButton>
            )}
            <Typography className={classes.title} variant="h6">
              <span className="currency">{ t('currency') }</span>
              { t('exchange') }
            </Typography>
            {isDesktop && (
              <>
                <NavigationLink
                  to="/"
                  active
                >
                  { t('currencyConverter') }
                </NavigationLink>
                <NavigationLink to="/history">
                  { t('viewConversionHistory') }
                </NavigationLink>
              </>
            )}
          </div>
          <div>
            <Dropdown
              label={t(langName)}
              className={classes.button}
              color="primary"
              startIcon={<TranslateIcon />}
              options={languageOptions}
              onUpdate={changeLanguage}
            />
            <IconButton
              color="primary"
              onClick={changeTheme}
            >
              { storedTheme === 'light' ? <Brightness4Icon /> : <Brightness7Icon />}
            </IconButton>
          </div>
        </div>
      </AppBar>
      {!isDesktop && (
        <Drawer
          open={showDrawer}
          toggle={setDrawerVisibility}
        />
      )}
    </div>
  );
}
