import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  navbar: {
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.text.main,
    display: 'flex',
    justifyContent: 'center',
  },
  toolbar: {
    padding: 0,
    width: '80%',
    margin: '0 auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: theme.spacing(8),
    '& > *': {
      display: 'flex',
      alignItems: 'center',
    },
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0, 2),
      width: '100%',
    },
  },
  title: {
    marginRight: theme.spacing(4),
    color: theme.palette.text.primary,
    '& > .currency': {
      color: theme.palette.tableHeader.main,
      fontWeight: 300,
    },
  },
  button: {
    color: theme.palette.primary.light,
    fontSize: theme.typography.caption.fontSize,
    fontWeight: 500,
  },
}));
