import { makeStyles } from '@material-ui/core';

export default makeStyles((theme) => ({
  root: {
    width: '80%',
    margin: '0 auto',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      padding: theme.spacing(0, 2),
    },
  },
}));
