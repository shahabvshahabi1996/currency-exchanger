import React from 'react';
import PropTypes from 'prop-types';
import useStyles from '@/components/common/Content/styles.content';

export default function Content({ children }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {children}
    </div>
  );
}

Content.propTypes = {
  children: PropTypes.node.isRequired,
};
