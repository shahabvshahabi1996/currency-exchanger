/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { IconButton, useMediaQuery, useTheme } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default function Dropdown({
  label,
  startIcon,
  options,
  onUpdate,
  ...rest
}) {
  const [anchorEl, setAnchorEl] = useState(null);
  const themeVariable = useTheme();
  const isDesktop = useMediaQuery(themeVariable.breakpoints.up('md'));

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      {
        isDesktop
          ? (
            <Button
              startIcon={startIcon}
              endIcon={<ExpandMoreIcon />}
              onClick={handleClick}
              {...rest}
            >
              { label }
            </Button>
          )
          : (
            <IconButton
              onClick={handleClick}
              {...rest}
            >
              { startIcon }
              <ExpandMoreIcon />
            </IconButton>
          )
      }
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {options.map(
          ({ title, value }) => (
            <MenuItem
              key={title}
              onClick={() => onUpdate(value)}
            >
              {title}
            </MenuItem>
          ),
        )}
      </Menu>
    </div>
  );
}

Dropdown.propTypes = {
  label: PropTypes.string.isRequired,
  startIcon: PropTypes.node,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  onUpdate: PropTypes.func,
};

Dropdown.defaultProps = {
  startIcon: <></>,
  onUpdate: () => {},
};
