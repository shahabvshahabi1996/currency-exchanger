/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import ButtonComponent from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import withStyles from '@/components/common/Button/styles.button';

function Button({ children, rounded, ...rest }) {
  if (rounded) {
    return (
      <IconButton
        {...rest}
      >
        {children}
      </IconButton>
    );
  }

  return (
    <ButtonComponent
      {...rest}
    >
      {children}
    </ButtonComponent>
  );
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  rounded: PropTypes.bool,
};

Button.defaultProps = {
  rounded: false,
};

export default withStyles(Button);
