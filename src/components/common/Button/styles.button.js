import { withStyles } from '@material-ui/core';

export default (component) => withStyles((theme) => ({
  root: {
    boxShadow: theme.shadows[2],
    ...(theme.palette.type === 'dark'
      ? {
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.primary,
      }
      : {}),
    '&.MuiButton-containedPrimary:hover': {
      ...(theme.palette.type === 'dark'
        ? {
          backgroundColor: theme.palette.background.paper,
        }
        : {}),
    },
    '&.MuiIconButton-root, &.Mui-focusVisible': {
      backgroundColor: theme.palette.background.paper,
      borderRadius: 2,
      lineHeight: 1.75,
      padding: 8,
    },
    '&.MuiButton-outlined': {
      backgroundColor: theme.palette.background.paper,
      border: 'none',
    },
    '&.danger': {
      color: theme.palette.warn.main,
    },
  },
}))(component);
