import React, { useMemo } from 'react';
import { ThemeProvider as MUIThemeProvider, createTheme } from '@material-ui/core';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';
import { create } from 'jss';
import rtl from 'jss-rtl';
import PropTypes from 'prop-types';
import { getCurrentLang, getDirection } from '@/i18n';
import { useStateToProps } from '@/redux/actions';
import themeConfig from '@/configs/theme-config';

const ThemeProvider = ({ children }) => {
  const storedTheme = useStateToProps((state) => state.settings.theme);
  const lang = getCurrentLang();
  const dir = getDirection(lang);

  const theme = useMemo(() => createTheme({
    ...themeConfig,
    palette: {
      ...themeConfig.palette,
      type: storedTheme,
    },
    typography: {
      ...themeConfig.typography,
      ...(dir === 'rtl'
        ? {
          fontFamily: '"Sahel", sans-serif',
          caption: {
            fontSize: 16,
            fontWeight: 700,
          },
        }
        : {
          fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        }
      ),
    },
  }), [storedTheme, dir]);

  theme.direction = dir;
  document.querySelector('body').setAttribute('dir', dir);

  if (dir === 'rtl') {
    const rtlJssStyles = create({ plugins: [...jssPreset().plugins, rtl()] });
    return (
      <StylesProvider jss={rtlJssStyles}>
        <MUIThemeProvider theme={theme}>
          {children}
        </MUIThemeProvider>
      </StylesProvider>
    );
  }

  return (
    <MUIThemeProvider theme={theme}>
      {children}
    </MUIThemeProvider>
  );
};

ThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ThemeProvider;
