/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import withStyles from '@/components/formElements/Input/styles.input';

function Input({ children, ...rest }) {
  return (
    <TextField {...rest}>
      {children}
    </TextField>
  );
}

Input.propTypes = {
  children: PropTypes.node,
};

Input.defaultProps = {
  children: <></>,
};

export default withStyles(Input);
