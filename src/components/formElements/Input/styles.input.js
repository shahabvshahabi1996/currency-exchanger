import { withStyles } from '@material-ui/core';

export default (component) => withStyles((theme) => ({
  root: {
    '& label': {
      color: theme.palette.text.main,
    },
    '& div.MuiInput-root': {
      color: theme.palette.text.main,
    },
    '& label.Mui-focused': {
      color: theme.palette.text.main,
    },
    '& .MuiInput-underline:hover:not(.Mui-disabled):before': {
      borderColor: theme.palette.primary.main,
    },
    '& .MuiInput-underline:after, & .MuiInput-underline:before, & .MuiInput-underline:hover': {
      borderBottomColor: theme.palette.text.main,
    },
    '& .MuiOutlinedInput-root': {
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.text.main,
      },
    },
  },
}))(component);
