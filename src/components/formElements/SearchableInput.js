/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import PropTypes from 'prop-types';
import Input from '@/components/formElements/Input/Input';

export default function SearchableInput({
  onChange,
  fieldValue,
  getProp,
  options,
  ...rest
}) {
  const [inputValue, setInputValue] = useState('');
  return (
    <Autocomplete
      getOptionSelected={() => true}
      onChange={(e, value) => {
        if (value !== null) onChange(value);
        else onChange({});
      }}
      value={fieldValue}
      getOptionLabel={(option) => option[getProp] || ''}
      onInputChange={(e, value) => setInputValue(value)}
      inputValue={inputValue}
      options={options}
      renderInput={(params) => (
        <Input
          {...params}
          {...rest}
        />
      )}
    />
  );
}

SearchableInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  fieldValue: PropTypes.objectOf(PropTypes.any).isRequired,
  getProp: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.any).isRequired,
};
