/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import RadioComponent from '@material-ui/core/Radio';
import withStyles from '@/components/formElements/Radio/styles.radio';

function Radio({ ...rest }) {
  return (
    <RadioComponent
      color="primary"
      {...rest}
    />
  );
}

export default withStyles(Radio);
