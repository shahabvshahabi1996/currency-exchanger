import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { useMediaQuery, useTheme } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import api from '@/api';
import queryMaker from '@/utils/queryMaker';
import useLoading from '@/hooks/use-loading';
import { dateFormat, subtract } from '@/utils/datetime';
import Input from '@/components/formElements/Input/Input';
import Radio from '@/components/formElements/Radio/Radio';
import Table from '@/components/common/Table/Table';
import SparkLine from '@/components/common/SparkLine';

export default function ExchangeHistory({ from, to }) {
  const { t } = useTranslation();
  const setLoading = useLoading();
  const theme = useTheme();
  const isNotMobile = useMediaQuery(theme.breakpoints.up('sm'));
  const durations = [7, 14, 30];
  const [selectedDuration, setDuration] = useState(durations[0]);
  const [view, setView] = useState(0);
  const [exchangeRates, setExchangeRates] = useState([]);
  const [statistics, setStatistics] = useState([]);
  const [chartData, setChartData] = useState({});

  const rateFieldsConfig = [
    {
      id: 1,
      name: 'timestamp',
      title: t('date'),
    },
    {
      id: 2,
      name: 'rate',
      title: t('exchangeRate'),
    },
  ];

  const statisticsFieldsConfig = [
    {
      id: 1,
      name: 'statistics',
      title: t('statistics'),
    },
    {
      id: 2,
      name: 'rate',
      title: '',
    },
  ];

  const calculateChartProperties = useCallback((result) => {
    const list = result.map((item) => item.rate);
    const labels = result.map((item) => item.timestamp);
    setChartData({ list, labels });
  }, []);

  const calculateStatistics = useCallback((ratesArr) => {
    const highest = Math.max(...ratesArr);
    const lowest = Math.min(...ratesArr);
    const average = ratesArr
      .reduce((prev, current) => prev + current, 0) / ratesArr.length;
    const obj = { highest, lowest, average: average.toLocaleString('en', { maximumFractionDigits: 6 }) };

    setStatistics(() => Object.keys(obj).map((key, index) => ({
      id: index,
      statistics: t(key),
      rate: obj[key],
    })));
  }, [t]);

  const loadExchangeHistory = useCallback(() => {
    setLoading(true);
    const start = subtract(new Date().setHours(0, 0, 0, 0), selectedDuration).toISOString();

    const query = queryMaker({
      currency: to.currency,
      start,
    });

    api
      .conversion
      .getExchangeHistory(`?${query}`)
      .then((res) => {
        if (res.data) return res;
        return res.update();
      })
      .then((res) => {
        const result = (res?.data || []).reverse().map((item, index) => ({
          ...item,
          id: index,
          rate: (Number(from.rate) / Number(item.rate))
            .toLocaleString('en', { maximumFractionDigits: 6 }),
          timestamp: dateFormat(item.timestamp),
        }));

        const ratesArr = result.map((item) => Number(item.rate));
        calculateStatistics(ratesArr);
        calculateChartProperties(result);
        setExchangeRates(result);
      })
      .finally(() => setLoading(false));
  }, [
    from?.rate,
    to?.currency,
    selectedDuration,
    setLoading,
    calculateStatistics,
    calculateChartProperties,
  ]);

  useEffect(() => {
    loadExchangeHistory();
  }, [loadExchangeHistory]);

  return (
    <div className="margin-md-top">
      <Typography variant="subtitle1">
        {t('exchangeHistory')}
      </Typography>
      <div className="margin-md-top">
        <Grid
          container
          spacing={3}
          alignItems="flex-end"
        >
          <Grid
            item
            sm={3}
            xs={6}
          >
            <Input
              select
              label={t('duration')}
              value={selectedDuration}
              onChange={(e) => setDuration(e.target.value)}
              fullWidth
            >
              {durations.map((duration) => (
                <MenuItem key={duration} value={duration}>
                  {t('days', [duration])}
                </MenuItem>
              ))}
            </Input>
          </Grid>
          {isNotMobile && <Grid item sm={2} />}
          <Grid
            item
            sm={6}
            xs={6}
          >
            <RadioGroup
              row
              aria-label="gender"
              value={view}
              onChange={(e) => setView(Number(e.target.value))}
            >
              <FormControlLabel value={0} control={<Radio />} label={t('table')} />
              <FormControlLabel value={1} control={<Radio />} label={t('chart')} />
            </RadioGroup>
          </Grid>
        </Grid>
      </div>
      <div className="margin-md-top">
        <Grid
          container
          spacing={3}
        >
          { view === 0 ? (
            <>
              <Grid
                item
                sm={6}
                xs={12}
              >
                <Table
                  fieldsConfig={rateFieldsConfig}
                  list={exchangeRates}
                />
              </Grid>
              <Grid
                item
                sm={6}
                xs={12}
              >
                <Table
                  fieldsConfig={statisticsFieldsConfig}
                  list={statistics}
                />
              </Grid>
            </>
          ) : (
            <Grid item xs={12}>
              <SparkLine
                labels={chartData?.labels || []}
                list={chartData?.list || []}
              />
            </Grid>
          )}
        </Grid>
      </div>
    </div>
  );
}

ExchangeHistory.propTypes = {
  to: PropTypes.objectOf(PropTypes.any).isRequired,
  from: PropTypes.objectOf(PropTypes.any).isRequired,
};
