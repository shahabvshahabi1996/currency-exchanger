import { makeStyles } from '@material-ui/core';

export default makeStyles((theme) => ({
  root: {
    '& *': {
      letterSpacing: 1.5,
      color: theme.palette.tableHeader.main,
    },
    '& > h1': {
      fontWeight: 100,
      verticalAlign: 'middle',
      '& > .result': {
        fontWeight: 500,
        color: theme.palette.accent.main,
      },
      [theme.breakpoints.down('sm')]: {
        fontSize: 30,
      },
    },
  },
}));
