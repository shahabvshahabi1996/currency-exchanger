import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import useStyles from '@/components/pages/converter/ConversionResult/styles.converter-result';

export default function ConversionResult({ amount, from, to }) {
  const classes = useStyles();
  const conversionRate = (Number(from.rate) / Number(to.rate));
  const reverseConversionRate = (Number(to.rate) / Number(from.rate));
  const converted = (Number(amount) * conversionRate).toLocaleString('en-US', { maximumFractionDigits: 4 });

  return (
    <div className={`${classes.root} text-center margin-xlg-top`}>
      <Typography variant="h1">
        <span>
          {amount}
          {from.currency}
        </span>
        <span> = </span>
        <span className="result">
          {converted}
          {to.currency}
        </span>
      </Typography>
      <div className="margin-lg-top">
        <Typography variant="body2">
          <span>
            1
            {from.currency}
          </span>
          <span> = </span>
          <span>
            {conversionRate.toLocaleString('en-US')}
            {to.currency}
          </span>
        </Typography>
        <Typography variant="body2">
          <span>
            1
            {to.currency}
          </span>
          <span> = </span>
          <span>
            {reverseConversionRate.toLocaleString('en-US', { maximumFractionDigits: 5 })}
            {from.currency}
          </span>
        </Typography>
      </div>
    </div>
  );
}

ConversionResult.propTypes = {
  amount: PropTypes.string,
  from: PropTypes.objectOf(PropTypes.any),
  to: PropTypes.objectOf(PropTypes.any),
};

ConversionResult.defaultProps = {
  amount: 0,
  from: { currency: '', rate: 1 },
  to: { currency: '', rate: 1 },
};
