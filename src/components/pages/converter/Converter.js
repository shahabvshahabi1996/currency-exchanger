/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect, useCallback } from 'react';
import { useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import CompareArrowsIcon from '@material-ui/icons/CompareArrows';
import Typography from '@material-ui/core/Typography';
import api from '@/api';
import useLoading from '@/hooks/use-loading';
import SearchableInput from '@/components/formElements/SearchableInput';
import Input from '@/components/formElements/Input/Input';
import Button from '@/components/common/Button/Button';

export default function Converter({ onSubmit }) {
  const { t } = useTranslation();
  const query = new URLSearchParams(useLocation().search);
  const [amount, setAmount] = useState(query?.get('amount') || '1');
  const [from, setFrom] = useState(JSON.parse(query?.get('from')) || {});
  const [to, setTo] = useState(JSON.parse(query?.get('to')) || {});
  const [rates, setExchangeRate] = useState([]);
  const setLoading = useLoading();

  const loadExchangeRates = useCallback(() => {
    setLoading(true);
    api
      .conversion
      .getExchangeRates()
      .then((res) => {
        if (res.data) return res;
        return res.update();
      })
      .then((res) => {
        setExchangeRate(res.data);
      })
      .finally(() => setLoading(false));
  }, [setLoading]);

  useEffect(() => {
    loadExchangeRates();
  }, [loadExchangeRates]);

  const swapFromTo = () => {
    setTo(from);
    setFrom(to);
  };

  const amountValidator = (value) => (value <= 0 ? t('errorMessages.positiveValue') : '');
  const isFormValidated = () => {
    if (amountValidator(amount)) return false;
    if (!from.currency) return false;
    if (!to.currency) return false;

    return true;
  };

  const submit = (e) => {
    e.preventDefault();
    onSubmit({ amount, from, to });
  };

  return (
    <form
      onSubmit={submit}
    >
      <Grid
        container
        spacing={3}
        alignItems="flex-start"
      >
        <Grid item md={2} xs={12}>
          <Input
            error={Boolean(amountValidator(amount))}
            label={t('amount')}
            type="number"
            name="amount"
            value={amount}
            onChange={(event) => setAmount(event.target.value)}
            helperText={amountValidator(amount)}
            fullWidth
            required
          />
        </Grid>
        <Grid item md={4} xs={5}>
          <SearchableInput
            onChange={(value) => setFrom(value)}
            fieldValue={from}
            getProp="currency"
            options={rates}
            label={t('from')}
            name="from"
            fullWidth
            required
          />
        </Grid>
        <Grid item md={1} xs={2} className="center-flex">
          <Button
            type="button"
            onClick={swapFromTo}
            color="primary"
            variant="outlined"
            rounded
          >
            <CompareArrowsIcon />
          </Button>
        </Grid>
        <Grid item md={4} xs={5}>
          <SearchableInput
            onChange={(value) => setTo(value)}
            fieldValue={to}
            getProp="currency"
            options={rates}
            label={t('to')}
            name="to"
            fullWidth
            required
          />
        </Grid>
        <Grid item md={1} xs={12}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            disabled={!isFormValidated()}
          >
            <Typography variant="body2">{t('convert')}</Typography>
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

Converter.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};
