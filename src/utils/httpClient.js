export default function httpClient(method, url, data, headers) {
  const fetchOptions = {
    method,
    headers,
  };

  if (method !== 'GET') fetchOptions.body = JSON.stringify(data);

  return fetch(url, fetchOptions);
}
