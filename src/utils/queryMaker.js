export default function queryMaker(obj) {
  if (!Object.keys(obj)) return '';
  return Object.keys(obj).map((key) => `${key}=${obj[key]}`).join('&');
}
