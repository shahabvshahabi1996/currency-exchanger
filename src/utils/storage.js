export const getItem = (key) => localStorage.getItem(key);
export const setItem = (key, value) => localStorage.setItem(key, value);
export const removeItem = (key) => localStorage.removeItem(key);
export const cache = {
  set(key, val) {
    sessionStorage.setItem(key, JSON.stringify(val));
  },
  get(key) {
    const ret = sessionStorage.getItem(key);
    return ret ? JSON.parse(ret) : null;
  },
};
