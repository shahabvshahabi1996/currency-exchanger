import moment from 'moment';

export const dateFormat = (value, format = 'DD/MM/YYYY') => moment(value).format(format);
export const subtract = (
  value = new Date(),
  duration,
  unit = 'days',
) => moment(value).subtract(duration, unit);
