import { useDispatchToProps } from '@/redux/actions';
import { SET_LOADING } from '@/redux/actions/action-types';

export default function useLoading() {
  return useDispatchToProps(SET_LOADING);
}
