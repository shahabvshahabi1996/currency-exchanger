import httpClient from '@/utils/httpClient';
import { cache } from '@/utils/storage';
import queryMaker from '@/utils/queryMaker';

const baseUrl = 'https://api.nomics.com/v1';

const request = (method = 'GET', fromCache = true) => (
  url,
  data = {},
  headers = {},
) => {
  const caching = method === 'GET' && fromCache;
  const responseInterface = (response, updateFn) => ({
    ...(response || {}),
    version: caching ? 'cache' : 'network',
    update: updateFn || (() => Promise.resolve()),
  });

  let currentURL = url.startsWith('http') ? url : `${baseUrl}${url}`;
  if (!currentURL.includes('key')) {
    const parameters = {
      key: process.env.REACT_APP_NOMICS_API_KEY,
    };
    const query = `${currentURL.includes('?') ? '&' : '?'}${queryMaker(parameters)}`;
    currentURL = `${currentURL}${query}`;
  }

  if (caching) {
    const cacheVersion = cache.get(currentURL) || undefined;
    return Promise.resolve(
      responseInterface(
        cacheVersion,
        request(method, false).bind(method, currentURL, data, headers),
      ),
    );
  }

  return new Promise((resolve, reject) => {
    httpClient(method, currentURL, data, headers)
      .then((res) => res.text().then((_data) => {
        const resultHeaders = {};
        const { status } = res;
        const { statusText } = res;
        const isOk = status >= 200 && status < 300;
        // eslint-disable-next-line no-restricted-syntax
        for (const section of res.headers.entries()) {
          const [key, value] = section;
          resultHeaders[key] = value;
        }

        let resultData = _data;
        if (
          resultHeaders['content-type']
          && resultHeaders['content-type'].includes('application/json')
        ) resultData = JSON.parse(_data || '{}');

        const result = {
          data: resultData,
          headers: resultHeaders,
          status,
          statusText,
        };

        if (method === 'GET' && isOk) cache.set(currentURL, result);
        if (isOk) resolve(responseInterface(result));
        else reject(responseInterface(result));
      }))
      .catch(reject);
  });
};

export default {
  get: request('GET'),
  post: request('POST'),
};
