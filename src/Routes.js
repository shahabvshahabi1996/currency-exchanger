import React, { Suspense, lazy } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import history from '@/utils/history';

const CurrencyConverterPage = lazy(() => import('@/pages/CurrencyConverter'));
const ConversionHistoryPage = lazy(() => import('@/pages/ConversionHistory'));
const NotFoundPage = lazy(() => import('@/pages/NotFound'));

export default function Routes() {
  return (
    <Router history={history}>
      <Suspense fallback={<CircularProgress />}>
        <Switch>
          <Route path="/" exact component={CurrencyConverterPage} />
          <Route path="/history" component={ConversionHistoryPage} />
          <Route path="*" component={NotFoundPage} />
        </Switch>
      </Suspense>
    </Router>
  );
}
