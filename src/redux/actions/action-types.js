// eslint-disable-next-line import/prefer-default-export
export const SET_THEME = 'SET_THEME';
export const SET_LOADING = 'SET_LOADING';
export const SET_HISTORY = 'SET_HISTORY';
export const DELETE_HISTORY = 'DELETE_HISTORY';
