/* eslint-disable import/prefer-default-export */
import { useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

export function useDispatchToProps(type) {
  const dispatch = useDispatch();
  const handleDispatch = (payload) => {
    dispatch({ type, payload });
  };
  return useCallback(handleDispatch, [type, dispatch]);
}

export function useStateToProps(selection, equalityFn = shallowEqual) {
  const handleState = useSelector(selection, equalityFn);
  return handleState;
}
