import { getItem, setItem } from '@/utils/storage';
import { SET_HISTORY, DELETE_HISTORY } from '@/redux/actions/action-types';

const initialState = {
  history: getItem('history') ? JSON.parse(getItem('history')) : [],
};

const addExchangeHistory = (oldHistory, payload) => {
  const newRecords = [...oldHistory, payload];
  setItem('history', JSON.stringify(newRecords));
  return newRecords;
};

const deleteExchangeHistory = (oldHistory, id) => {
  const newRecords = oldHistory.filter((record) => record.id !== id);
  setItem('history', JSON.stringify(newRecords));
  return newRecords;
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_HISTORY:
      return {
        ...state,
        history: addExchangeHistory(state.history, action.payload),
      };
    case DELETE_HISTORY:
      return {
        ...state,
        history: deleteExchangeHistory(state.history, action.payload),
      };
    default:
      return state;
  }
};
