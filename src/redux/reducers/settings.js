import { getItem, setItem } from '@/utils/storage';
import { SET_LOADING, SET_THEME } from '@/redux/actions/action-types';
import { getCurrentLang } from '@/i18n';

const initialState = {
  theme: getItem('theme') || 'light',
  lang: getCurrentLang(),
  isLoading: false,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_THEME:
      setItem('theme', action.payload);
      return {
        ...state,
        theme: action.payload,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      return state;
  }
};
