import { combineReducers } from 'redux';
import settings from '@/redux/reducers/settings';
import exchanges from '@/redux/reducers/exchanges';

export default combineReducers({
  settings,
  exchanges,
});
