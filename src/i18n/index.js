import { initReactI18next } from 'react-i18next';
import i18n from 'i18next';
import history from '@/utils/history';
import { getItem, setItem } from '@/utils/storage';
import en from '@/i18n/en';
import fa from '@/i18n/fa';

const debug = process.env.NODE_ENV === 'development';

const languages = {
  en: {
    name: 'english',
    direction: 'ltr',
  },
  fa: {
    name: 'farsi',
    direction: 'rtl',
  },
};

function getCurrentLang() {
  return getItem('lang') || 'en';
}

function getDirection(lang) {
  return languages[lang]?.direction || 'ltr';
}

function getLanguageName(lang) {
  return languages[lang]?.name || '';
}

function setLanguage(lang) {
  setItem('lang', lang);
  i18n.changeLanguage(lang);
  history.go(0);
}

const fallbackLng = getCurrentLang();

i18n
  .use(initReactI18next)
  .init({
    fallbackLng,
    debug,
    resources: {
      en: {
        translation: en,
      },
      fa: {
        translation: fa,
      },
    },
  });

export {
  setLanguage,
  getCurrentLang,
  getDirection,
  getLanguageName,
  languages,
};
export default i18n;
