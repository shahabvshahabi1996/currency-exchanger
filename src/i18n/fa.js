export default {
  actions: 'عملیات',
  amount: 'مقدار',
  average: 'متوسط',

  chart: 'نمودار',
  convert: 'تبدیل کردن',
  currency: 'ارز',
  currencyConverter: 'تبدیل کننده ارز',
  currencyConversion: 'تبدیل ارز',

  date: 'تاریخ',
  days: '{{0}} روز',
  delete: 'حذف کردن',
  duration: 'زمان',

  english: 'انگلیسی',
  event: 'رویداد',
  exchange: 'تبادل',
  exchangeHistory: 'تاریخجه تبادل',
  exchangeHistoryEvent: 'مقدار {{0}} از {{1}} به {{2}} تبدیل شد',
  exchangeRate: 'نرخ تبادل',

  farsi: 'فارسی ',
  from: 'از',

  iWantToConvert: 'میخواهم تبدیل کنم',

  highest: 'بیشترین',

  lowest: 'کمترین',

  notFound: '404 صفحه ای با این آدرس پیدا نشد.',

  statistics: 'آمار',

  table: 'جدول',
  to: 'به',

  view: 'دیدن',
  viewConversionHistory: 'مشاهده تاریخچه تبدیل',

  errorMessages: {
    positiveValue: 'لطفا مقداری مثبتی را وارد نمایید',
  },
};
