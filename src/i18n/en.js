export default {
  actions: 'Actions',
  amount: 'Amount',
  average: 'Average',

  chart: 'Chart',
  convert: 'Convert',
  currency: 'Currency',
  currencyConverter: 'CURRENCY CONVERTER',
  currencyConversion: 'Currency Conversion',

  date: 'Date',
  days: '{{0}} Days',
  delete: 'Delete',
  duration: 'Duration',

  english: 'English',
  event: 'Event',
  exchange: 'Exchange',
  exchangeHistory: 'Exchange History',
  exchangeHistoryEvent: 'Converted an amount of {{0}} from {{1}} to {{2}}',
  exchangeRate: 'Exchange Rate',

  farsi: 'Farsi',
  from: 'From',

  iWantToConvert: 'I want to convert',

  highest: 'Highest',

  lowest: 'Lowest',

  notFound: '404 Page Not Found',

  statistics: 'Statistics',

  table: 'Table',
  to: 'To',

  view: 'View',
  viewConversionHistory: 'VIEW CONVERSION HISTORY',

  errorMessages: {
    positiveValue: 'Please insert a positive value',
  },
};
