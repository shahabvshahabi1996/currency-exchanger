# 💱 Currency Exchanger

This is a fast, efficient, and responsive currency converter web application built with `React` and `Material UI` components Using the `nomics currency exchanger API`.

## 🔑 Features

- [X]  Responsive web pages.
- [X]  Using `lazy-loading` for code splitting routes.
- [X]  Using `Material UI` UI components.
- [X]  Using `i18n` for localization.
- [X]  Using `redux` as global state manager.
- [X]  Storing API response to reduce `redundant` Network request.
- [X]  Dark mode support.
- [X]  RTL support.
- [X]  Using Absolute path.

## 📝 App structure

```bash
├── README.md
├── craco.config.js
├── jsconfig.json
├── package-lock.json
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
└── src
    ├── App.js
    ├── App.test.js
    ├── Boot.js
    ├── Routes.js
    ├── api
    │   ├── conversion.js
    │   └── index.js
    ├── assets
    │   ├── global-css
    │   │   └── index.css
    │   └── logo.svg
    ├── components
    │   ├── common
    │   │   ├── Button
    │   │   │   ├── Button.js
    │   │   │   └── styles.button.js
    │   │   ├── Content
    │   │   │   ├── Content.js
    │   │   │   └── styles.content.js
    │   │   ├── Drawer
    │   │   │   ├── Drawer.js
    │   │   │   └── styles.drawer.js
    │   │   ├── Dropdown.js
    │   │   ├── Layout
    │   │   │   ├── Layout.js
    │   │   │   └── styles.layout.js
    │   │   ├── LinearProgress
    │   │   │   ├── LinearProgress.js
    │   │   │   └── styles.linear-progress.js
    │   │   ├── Navbar
    │   │   │   ├── Navbar.js
    │   │   │   └── styles.navbar.js
    │   │   ├── NavigationLink
    │   │   │   ├── NavigationLink.js
    │   │   │   └── styles.navigation-link.js
    │   │   ├── Sparkline.js
    │   │   └── Table
    │   │       ├── Table.js
    │   │       └── styles.table.js
    │   ├── formElements
    │   │   ├── Input
    │   │   │   ├── Input.js
    │   │   │   └── styles.input.js
    │   │   ├── Radio
    │   │   │   ├── Radio.js
    │   │   │   └── styles.radio.js
    │   │   └── SearchableInput.js
    │   ├── pages
    │   │   ├── converter
    │   │   │   ├── ConversionResult
    │   │   │   │   ├── ConversionResult.js
    │   │   │   │   └── styles.converter-result.js
    │   │   │   ├── Converter.js
    │   │   │   └── ExchangeHistory.js
    │   │   └── history
    │   └── providers
    │       └── ThemeProvider.js
    ├── configs
    │   └── theme-config.js
    ├── hooks
    │   └── use-loading.js
    ├── i18n
    │   ├── en.js
    │   ├── fa.js
    │   └── index.js
    ├── index.js
    ├── pages
    │   ├── ConversionHistory.js
    │   └── CurrencyConverter.js
    ├── redux
    │   ├── actions
    │   │   ├── action-types.js
    │   │   └── index.js
    │   ├── reducers
    │   │   ├── exchanges.js
    │   │   ├── index.js
    │   │   └── settings.js
    │   └── store.js
    ├── reportWebVitals.js
    ├── service
    │   └── request.js
    ├── setupTests.js
    └── utils
        ├── datetime.js
        ├── history.js
        ├── httpClient.js
        ├── queryMaker.js
        └── storage.js
```

The `src/` directory includes these folders:
-  **api**: which keeps all the API routes.
-  **assets**: which includes all assets like (images, css files, ...).
-  **components**: which includes all components like (common, formElements, pages, providers).
-  **configs**: includes all configs like (theme-configs).
-  **hooks**: includes react hooks like (use-loading).
-  **i18n**: has all different locales like (en, fa, ...) and also the `i18n` config and utils.
-  **pages**: includes all application pages (CurrencyConverter, ConversionHistory).
-  **redux**: has all the redux main parts like (the store, actions and reducers).
-  **service**: it has all service like things like request which is the backbone for all Network request.
-  **utils**: like javascript functions to make the computation easier.

## 😎 How to initiate and start the project ?

simple, first clone it using `git clone the repo url`
then just hit `npm install` on your terminal and then hit `npm run dev`.

now your application is up and running on `http://localhost:3000`.

## 🙌 How to contribute ?

After reading instructions below you can fork and start contributing, please fill free to contribute and ask your questions about the project.

**Instructions**

- Please your define your merge request branches like so `[type]/[abstract information about your work separated by '-']`.
  - [type: feature]: to add new feature to project.
  - [type: chore]: to change existing feature or file.
  - [type: bugfix]: to fix a bug on the project.
- Please put all your assets and global css files into `src/assets` directory.
- Please place all your pages on `src/pages`.
- Please define your component Pascal case like (`Navbar.js`).
  - If you need to style your component please make a directory with the name of your component and move your files into it.
  - For naming your styles please use this pattern `styles.component-name.js`
- Please put generic components to `src/components/common`.
- Please put all form related components to `src/components/formElements`.
- Please put your page related components to `src/components/pages`.
- Please put providers into `src/components/providers`.
- Put your hooks into `src/hooks` directory and define your file name using `use-[feature-all-lowercase].js` pattern.
- Define your routes on `src/Routes.js` file and use `lazy-loading` for route base code splitting.
- **ALWAYS** use **absolute** paths.